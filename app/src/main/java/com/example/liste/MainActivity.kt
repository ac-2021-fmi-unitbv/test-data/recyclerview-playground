package com.example.liste

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.liste.adapters.MyAdapter
import com.example.liste.models.Movie

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpRecyclerView()
    }

    fun setUpRecyclerView() {
        val recyclerView: RecyclerView = findViewById(R.id.list)
        val layoutManager: LinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        val movies: ArrayList<Movie> = ArrayList<Movie>()
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        movies.add(Movie("Titanic","Romance"))
        val adapter = MyAdapter(movies)
        recyclerView.adapter=adapter

    }

}