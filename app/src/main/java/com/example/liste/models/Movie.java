package com.example.liste.models;

public class Movie {
   private String title;
   private String subtitle;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }
   public Movie(String title,String subtitle)
    {
        this.title=title;
        this.subtitle=subtitle;
    }
}
